libmixin-extrafields-perl (0.140003-1) unstable; urgency=medium

  * Import upstream version 0.140003.
  * Update years of upstream copyright.
  * Update upstream email address.
  * Update debian/upstream/metadata.
  * Update alternative test dependencies.
  * Declare compliance with Debian Policy 4.6.2.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.
  * Drop internal comment from previous changelog entry.

 -- gregor herrmann <gregoa@debian.org>  Sat, 07 Jan 2023 20:12:54 +0100

libmixin-extrafields-perl (0.140002-1) unstable; urgency=low

  [ Florian Schlichting ]
  * Import Upstream version 0.140002
  * Drop fix-bad-whatis.patch, applied upstream

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Fabrizio Regalli from Uploaders. Thanks for your work!
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Remove constraints unnecessary since stretch:
    + Build-Depends-Indep: Drop versioned constraint on libtest-simple-perl and
      perl.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 15 Jun 2022 19:36:29 +0100

libmixin-extrafields-perl (0.140001-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sat, 09 Jan 2021 16:11:48 +0100

libmixin-extrafields-perl (0.140001-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release (0.100971)

  [ Fabrizio Regalli ]
  * Add myself to Uploaders and Copyright.
  * Switch d/compat to 8.
  * Build-Depends: switch to debhelper (>= 8).
  * Bump to 3.0 quilt format.
  * Update link for license GPL-1 on d/copyright.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ Florian Schlichting ]
  * Import Upstream version 0.140001
  * Update year of upstream copyright
  * Remove copyright paragraph for absent inc/Module/*
  * Update stand-alone license paragraphs to commonly used version (not
    limiting Debian to GNU/Linux, directly linking to GPL-1)
  * Add build-dependency on Test::More 0.96
  * Declare compliance with Debian Policy 3.9.4
  * Added new fix-bad-whatis.patch
  * Add myself to uploaders and copyright

 -- Florian Schlichting <fsfs@debian.org>  Thu, 03 Oct 2013 21:24:12 +0200

libmixin-extrafields-perl (0.008-2) unstable; urgency=low

  * Add missing dependency on libstring-rewriteprefix-perl.
  * Set Standards-Version to 3.8.4 (no changes).

 -- gregor herrmann <gregoa@debian.org>  Fri, 12 Feb 2010 00:46:09 +0100

libmixin-extrafields-perl (0.008-1) unstable; urgency=low

  * New upstream release
  * Rewrite control description
  * Add myself to Uploaders and Copyright
  * Update years of copyright for M::I

 -- Jonathan Yu <jawnsy@cpan.org>  Sun, 24 Jan 2010 13:55:26 -0500

libmixin-extrafields-perl (0.007-1) unstable; urgency=low

  * Initial release (cf. #562102).

 -- gregor herrmann <gregoa@debian.org>  Wed, 23 Dec 2009 18:29:13 +0100
